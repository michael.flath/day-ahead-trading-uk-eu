
get_currency_data <- function(start_date, end_date) {
  start_date <- convert_date_character(as.character(as.POSIXct(
    ymd(start_date, tz = "CET"), origin = "1970-01-01") - days(3)))
  end_date <- convert_date_character(end_date)
  if (is.na(start_date) | is.na(end_date)) {
    stop("get_currency_exchange_rates: wrong date format! \n")
    # return()
  }
  # database connection
  initialize_pool()
  exchange_rates_tbl <- tbl(db_pool, in_schema("marketdata", "exchange_rates"))
  
  suppressWarnings({
    exchange_rate <- exchange_rates_tbl %>%
      filter(.data$timestamp >= start_date) %>%
      filter(.data$timestamp < end_date) %>%
      collect()
  })
  # query data
  if (plyr::empty(exchange_rate)) {
    cat("get_currency_exchange_rates: no data found! \n")
    # RMySQL::dbDisconnect(database_connection)
    # return(exchange_rate)
  } else {
    cat("get_currency_exchange_rates: data collected! \n")
  }
  # prepare data
  
  # exchange_rate <- exchange_rate %>%
  #   mutate(timestamp = ymd_hms(.data$timestamp, tz = "UTC"))
  
  exchange_rates_tmp <- exchange_rate %>%
    mutate(
      day = date(as.POSIXct(timestamp, origin = "1970-01-01")),
      exchange_rate = rate
    ) %>%
    select(day, sourceCurrency, targetCurrency, exchange_rate)
  
  
  # filter the last exchange rate and add it to actual date because rate is missing for actual date
  days_1 <- distinct(data.frame(date(seq(
    as.POSIXct(start_date, origin = "1970-01-01", tz = "CET"),
    as.POSIXct(end_date, origin = "1970-01-01", tz = "CET"), "days"
  )))) %>%
    mutate(
      sourceCurrency = "EUR",
      targetCurrency = "GBP",
      exchange_rate = NA_real_
    )
  
  colnames(days_1) <- c("day", "sourceCurrency", "targetCurrency", "exchange_rate")
  
  days_2 <- distinct(data.frame(date(seq(
    as.POSIXct(start_date, origin = "1970-01-01", tz = "CET"),
    as.POSIXct(end_date, origin = "1970-01-01", tz = "CET"), "days"
  )))) %>%
    mutate(
      sourceCurrency = "GBP",
      targetCurrency = "EUR",
      exchange_rate = NA_real_
    )
  
  colnames(days_2) <- c("day", "sourceCurrency", "targetCurrency", "exchange_rate")
  
  days <- days_1 %>%
    bind_rows(days_2) %>%
    arrange(day)
  
  
  exchange_rate_last <- exchange_rates_tmp$exchange_rate[nrow(exchange_rates_tmp)]
  
  exchange_rate <- exchange_rates_tmp %>%
    bind_rows(data.frame(day = date(end_date) - ddays(1), exchange_rate = exchange_rate_last)) %>%
    full_join(days, by = c("day", "sourceCurrency", "targetCurrency", "exchange_rate")) %>%
    arrange(sourceCurrency, targetCurrency, day) %>%
    distinct(day, sourceCurrency, targetCurrency, .keep_all = TRUE) %>%
    filter(!is.na(sourceCurrency) & !is.na(targetCurrency)) %>%
    pivot_wider(
      id_cols = "day", names_from = c(sourceCurrency, targetCurrency),
      values_from = exchange_rate
    ) %>%
    filter(day >= start_date) %>%
    fill(EUR_GBP, GBP_EUR) %>%
    filter(!is.na(EUR_GBP) & !is.na(GBP_EUR))
  
  # cat("get_currency_exchange_rates: disconnecting data base. \n")
  # RMySQL::dbDisconnect(database_connection)
  return(exchange_rate)
}


exchange_rates <- get_currency_data(start_date, end_date)

# day ahead prices --------------------------------------------------------

day_ahead_prices <- get_dayahead_price(c("UK", "FR", "NL", "BE"),
                                       start_date, end_date) %>%
  convert_time_columns_to_CET() %>%
  mutate(day = floor_date(delivery_start, unit = "days")) %>%
  left_join(exchange_rates, by = "day") %>%
  filter(type == "PT1H") %>%
  mutate(
    market_area = substr(market_area, 4, 5),
    exchange_rate = GBP_EUR,
    price = if_else(currency == "GBP", round(price * exchange_rate, 2), price)
  ) %>%
  select(delivery_start, market_area, price) %>%
  pivot_wider(id_cols = "delivery_start", names_from = market_area,
              values_from = price) %>%
  mutate(
    GB_FR = GB - FR,
    GB_NL = GB - NL,
    GB_BE = GB - BE
  )


# daily summary -----------------------------------------------------------

day_ahead_prices_daily <- day_ahead_prices %>%
  mutate(delivery_day = floor_date(delivery_start, unit = "days")) %>%
  group_by(delivery_day) %>%
  summarise(
    GB_FR = round(mean(GB_FR), 2),
    GB_NL = round(mean(GB_NL), 2),
    GB_BE = round(mean(GB_BE), 2)
  ) %>%
  ungroup()



# monthly summary -----------------------------------------------------------

day_ahead_prices_monthly <- day_ahead_prices %>%
  mutate(delivery_month = month(delivery_start)) %>%
  group_by(delivery_month) %>%
  summarise(
    GB_FR = round(mean(GB_FR), 2),
    GB_NL = round(mean(GB_NL), 2),
    GB_BE = round(mean(GB_BE), 2)
  ) %>%
  ungroup()


